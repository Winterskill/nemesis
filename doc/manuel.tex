% Nemesis
% Copyright (C) 2020  Soni Tourret
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

\documentclass[12pt, titlepage]{report}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[french]{babel}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{hyperref}

\definecolor{color_green}{rgb}{0,0.6,0}

\lstdefinestyle{codestyle}{
	commentstyle=\color{color_green},
	keywordstyle=\color{magenta},
	stringstyle=\color{purple},
	breakatwhitespace=false,
	breaklines=true,
	captionpos=b,
	keepspaces=true,
	numbers=left,
	numbersep=5pt,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	tabsize=2
}
\lstset{style=codestyle}

\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	filecolor=magenta,
	urlcolor=blue
}

\graphicspath{ {./images/} }

\title{Manuel}
\author{Soni Tourret}
\date{}

\begin{document}
\maketitle

\newpage

\part{Utiliser le framework}

	\chapter{Getting Started}

		\section{Votre premier jeu}

			Une fois le framework installé, vous pouvez commencer à coder votre premier jeu.

			Créez un nouveau fichier, qui sera le fichier principal de votre jeu. Nous allons l'appeler \verb|main.py|. Il existe deux manières de créer un jeu avec le framework. La première consiste à créer une instance de la classe \verb|Application|, et la deuxième à créer une classe fille de cette classe \verb|Application|. Ici, nous allons utiliser la première méthode, plus simple, mais la plupart des jeux complexes requierent d'utiliser la deuxième.

			\begin{lstlisting}[language=Python]
from nemesis.Application import *

if __name__ == '__main__':
	app = Application()
	app.execute()
			\end{lstlisting}

			A la ligne 1, nous importons le framework. A la ligne 4, nous créons une instance de \verb|Application|, et nous invoquons sa méthode \verb|execute| à la ligne 5.
	
		\section{Organisation des fichiers}

			Dans un jeu Nemesis, les fichiers sont organisés de cette façon~:

			\begin{itemize}
				\item[\textbf{/}] Le dossier-racine du jeu. C'est ici que vous placerez vos fichiers de code source.
				\item[\textbf{/nemesis/}] Le dossier qui comprend les fichiers-source du framework.
				\item[\textbf{/resources/}] Le dossier qui contient toutes les ressources du jeu (images, sons, maps...).
				\begin{itemize}
					\item[\textbf{sprites/}] Le dossier dans lequel seront placés les sprites du jeu. Le terme sprite comprend aussi bien les sprites des personnages que les tiles des tilesets. Chaque sprite est placé dans un dossier qui porte son nom, et toutes les images dedans sont interprétées comme les différentes variantes de ce sprite.
					\item[\textbf{tilesets/}] Le dossier dans lequel seront placés les tilesets du jeu.
					\item[\textbf{maps/}] Le dossier contenant les maps du jeu.
				\end{itemize}
			\end{itemize}
		
		\section{Ajouter une carte de base}

			Vous aurez remarqués que le jeu est entièrement vide. C'est parce que nous n'avons pas encore ajoutés de scène de jeu. Nous allons le faire maintenant.

			Nemesis gère les niveaux de jeu avec la classe \verb|GameScene|. Une \verb|GameScene| possède un nom, et peut posséder une \verb|Map|, qui gère l'affichage du décor. Sans \verb|Map|, la scène sera intégralement noire et vide, mais c'est possible de le faire. Nous allons commencer par faire ça.

			Reprenons notre code de départ, et créons une instance de \verb|GameScene|~:

			\begin{lstlisting}[language=Python]
from nemesis.Application import *
from nemesis.GameScene import *

if __name__ == '__main__':
	app = Application()

	maScene = GameScene("ma_scene", None)
	app.register_game_scene(maScene)
	app.set_current_game_scene("ma_scene")

	app.execute()
			\end{lstlisting}

			Nous avons donc créés une scène de jeu. Le premier paramètre du constructeur est le nom de la scène. Celui-ci sert à Nemesis pour savoir de quelle map il s'agit. Il ne peut pas y avoir plusieurs \verb|GameScene| enregistrées sous le même nom.

			Puis nous enregistrons la scène ainsi créée dans notre application, grâce à la méthode \verb|Application.register_game_scene|.

			Enfin, nous changons la scène actuelle pour notre scène nouvellement créée, grâce à la méthode \verb|Application.set_current_game_scene|, en la référant par son nom. A partir du moment où la scène est enregistrée dans le jeu, il faut y faire référence avec son nom.

			Nous avons maintenant une scène, mais rien dedans. Ajoutons donc un joueur.

		\section{Ajouter le joueur}
			
			Nemesis possède une classe spécialement conçue pour représenter le joueur~: la classe \verb|Player|. Nous allons l'utiliser.

			\begin{lstlisting}[language=Python]
from nemesis.Application import *
from nemesis.GameScene import *
from nemesis.Player import *
from nemesis.constants import *
from pygame.math import Vector2

if __name__ == '__main__':
	app = Application()

	player = Player()
	player.load_sprite("player")
	player.set_used_sprite("face")
	player.set_position(Vector2(G_CELL_WIDTH*5, G_CELL_HEIGHT*5))

	maScene = GameScene("ma_scene", None)
	maScene.register_entity(player)
	app.register_game_scene(maScene)
	app.set_current_game_scene("ma_scene")

	app.execute()
			\end{lstlisting}

			A la ligne 3, nous importons la classe \verb|PLayer|. A la ligne 4, nous importons le module \verb|constants|, qui contient les constantes globales du jeu. Nous en aurons besoin pour positionner le joueur sur la map. A la ligne 5, nous importons la classe \verb|Vector2| de pygame, qui est requise pour indiquer la position du joueur.

			A la ligne 10, nous créons notre instance de \verb|Player|. A la ligne suivante, nous définissons le sprite qu'il utilisera, avec la méthode \verb|Entity.load_sprite| (la classe \verb|Player| hérite de \verb|Entity|, qui représente toutes les entités du jeu). A la ligne d'après, nous utilisons la méthode \verb|Entity.set_used_sprite| pour définir quelle variante du sprite doit être utilisée actuellement. A la ligne 13, nous changons la position du joueur. Nous utilisons les constantes \verb|CELL_WIDTH| et \verb|CELL_HEIGHT|, qui représentent respectivement la largeur et la hauteur d'une case du jeu, pour positionner le joueur dans une case et non entre deux. Nous positionnons donc notre joueur à la case $\{5;5\}$ (la première case étant $\{0;0\}$).

			A la ligne 16, nous avons rajoutés un appel à la méthode \verb|GameScene.register_entity|, qui permet d'enregistrer une entité (le joueur étant une entité) dans la scène, afin d'autoriser la scène à l'afficher.

			Nous n'avons pas besoin d'en faire plus~: la classe \verb|Player| définit déjà les contrôles de direction du joueur (avec les touches directionnelles). Bien sûr, pour un joueur plus évolué, il faudra créer une classe personnalisée pour gérer au mieux le joueur, mais la classe \verb|Player| suffit amplement pour les prototypes de jeu.
		
		\section{Une carte simple}

			Nous avons à présent notre joueur qui s'ébat gaiement sur notre écran noir. Mais ce serait tout de même plus joli s'il voulait s'ébattre sur un décor, non ? C'est ce que nous allons faire.

			L'outil recommandé pour créer les cartes est l'éditeur de cartes Tiled. Vous pouvez également utiliser un autre outil, ou même coder la carte à la main, sachant que les cartes sont stockées dans des fichiers \verb|json|, mais sachez que le format utilisé est celui de Tiled.

			Si vous ne l'avez pas, commencez par obtenir Tiled. C'est un logiciel open source et gratuit, très flexible et en constante évolution. Vous pouvez l'obtenir sur son site~: \href{https://www.mapeditor.org/}{https://www.mapeditor.org/} .

			Une fois que vous l'avez installé, il va falloir créer deux choses~: un tileset et une carte. Commencons par créer le tileset.

			\subsection{Le tileset}

				Un tileset est une collection d'images qui seront utilisées pour composer la map.

				Cliquez sur \og Nouveau Tileset \fg{} (si vous lancez Tiled pour la première fois), ou sur \og Fichier \fg{} $\rightarrow$ \og Nouveau \fg{} $\rightarrow$ \og Nouveau tileset \fg{}.

				Dans la fenêtre qui s'ouvre, entrez le nom du tileset (celui que vous voulez).

				Tiled supporte deux types de tilesets~: \og Collection des images \fg{}, et \og Basé sur l'image du Tileset \fg{}. La première crée un tileset dont toutes les tiles (ses composants) sont issues de fichiers différents, et chaque fichier forme une tile. La deuxième utilise une seule grande image qui contient toutes les tiles, qu'elle découpe pour les en tirer. Nemesis ne supporte pour le moment que la première méthode. Sélectionnez donc \og Collection des images \fg{}. Si vous avez une carte ouverte dans Tiled et que la case à cocher \og Embarquer dans la carte \fg{} est cochable, décochez-la. Puis cliquez sur \og OK \fg{}.

				\begin{figure}[h]
					\centering
					\includegraphics[scale=0.6]{map_tileset.png}
				\end{figure}

				Quand vous cliquez sur \og OK \fg{}, Tiled ouvre une fenêtre pour savoir où enregistrer le fichier de tileset. Déplacez-vous jusqu'à l'emplacement de votre jeu, puis dans le dossier \og resources \fg{}, puis \og tilesets \fg{}. Enfin, donnez un nom à votre tileset (c'est ce nom qui sera utilisé par Nemesis pour faire référence au tileset) et enregistrez-le au format \og Fichier de tileset JSON (*.json) \fg{}.

				Vous arrivez alors sur une fenêtre vide. Pour ajouter une tile à votre tileset, cliquez sur le bouton bleu + dans la barre d'outils, puis choisissez le fichier de tile que vous voulez. Attention~: ne choisissez toujours qu'un fichier situé dans le dossier \og sprites \fg{} du dossier \og resources \fg{} de votre jeu, sans quoi Nemesis sera incapable de le reconnaitre.

				Une fois que vous avez composé votre tileset, il est temps de créer une map avec.

			\subsection{La carte}

				Cliquez sur \og Fichier \fg{} $\rightarrow$ \og Nouveau \fg{} $\rightarrow$ \og Nouvelle carte \fg{}.

				Dans la fenêtre qui s'ouvre, Tiled vous demande plusieurs choses.

				\begin{itemize}
					\item \og Orientation \fg{}~: L'orientation est le type de map que l'on souhaite créer~: orthogonale (carrée), isométrique ou hexagonale. Pour le moment, Nemesis ne supporte que les cartes orthogonales. Sélectionnez donc \og Orthogonale \fg{}.
					\item \og Format de calque de tile \fg{}~: C'est le format qui sera utiliser pour stocker les informations des tiles au sein du fichier. Laissez \og CSV \fg{}.
					\item \og Ordre d'affichage des tiles \fg{}~: Laissez \og En bas à droite \fg{}.
				\end{itemize}

				Dans la catégorie \og Taille de la map \fg{}, sélectionnez \og Fixe \fg{} (Nemesis ne supporte pas pour le moment les cartes infinies), et indiquez la largeur et la hauteur de votre carte en cases.

				Dans la catégorie \og Taille des éléments \fg{}, vous pouvez choisir la taille des tiles. Mettez la taille des cases que votre jeu utilise (par exemple 16 pixels par 16 pixels, 32 par 32, 64 par 64...).

				Puis cliquez sur \og Enregistrer sous \fg{}. Dans la fenêtre d'enregistrement que Tiled ouvre, déplacez vous dans le dossier \og resources \fg{} de votre jeu, puis dans le dossier \og maps \fg{}. Donnez à votre fichier le nom par lequel vous souhaitez que Nemesis référence la carte, puis enregistrez au format \og Fichiers de cartes JSON (*.json) \fg{}.

				Composez votre carte comme vous le souhaitez. Pour plus d'informations sur la façon d'utiliser Tiled, je vous conseille les tutoriels sur le site du logiciel.
			
			\subsection{Afficher la carte}

				Une fois que vous avez fini de composer votre carte, n'oubliez pas d'enregistrer, puis retourner dans votre code. Nous allons le modifier légèrement~:

				\begin{lstlisting}[language=Python]
from nemesis.Application import *
from nemesis.GameScene import *
from nemesis.Player import *
from nemesis.Map import Map
from nemesis.constants import *
from pygame.math import Vector2

if __name__ == '__main__':
	app = Application()

	player = Player()
	player.load_sprite("player")
	player.set_used_sprite("face")
	player.set_position(Vector2(G_CELL_WIDTH*5, G_CELL_HEIGHT*5))

	carte = Map("carte")

	maScene = GameScene("ma_scene", carte)
	maScene.register_entity(player)
	app.register_game_scene(maScene)
	app.set_current_game_scene("ma_scene")

	app.execute()
				\end{lstlisting}

				A la ligne 4, nous avons importés la classe \verb|Map|, qui va servir à représenter la carte de jeu. A la ligne 16, nous avons instanciés cette \verb|Map| dans une variable. Nous avons donnés en paramètre le nom de la carte. Enfin, nous avons modifiés la ligne 18, pour remplacer le \verb|None| par \verb|carte| pour dire à Nemesis d'utiliser cette carte pour cette scène.

				A présent, si vous lancez le jeu, vous verrez que la carte est affichée, et que le joueur gambade gaiement dessus.
		
		\section{Vous ne passerez pas~: les collisions de tiles}

			Dans l'état actuel des choses, il est possible de marcher partout, car les tiles n'ont pas de boîtes de collisions. Pour l'instant, la méthode utilisée par Nemesis pour les collisions de tiles est très simple~: il faut affecter aux tiles du tileset une propriété personnalisée nommée \verb|collisions_policy|, de type \verb|bool|. Si la propriété est décochée (par défaut, équivaut à ne pas la mettre), la tile n'aura pas de collision et les entités passeront à travers. Si elle est cochée, la tile aura une boîte de collision, et il sera impossible pour les entités de la traverser.
	
	\chapter{FAQ Technique}

		\section{Comment accéder à l'application ?}

			\textit{Quand on crée un jeu avec Nemesis, on instancie la classe} \verb|Application| \textit{Mais comment peut-on y accéder dans le code du jeu, par exemple dans une méthode d'une entité~?}

			Vous pouvez utiliser la constante globale \verb|APPLICATION.HANDLER|. Cette constante, qui est stockée dans le module \verb|nemesis.constants|, contient une référence vers l'instance active d'\verb|Application|.

			Par exemple, pour changer le titre du jeu quand l'entité est à une certaine position sur la map~:

			\begin{lstlisting}[language=Python]
from nemesis.Entity import Entity
from nemesis.Map import cell2pixels
from nemesis.constants import *

from pygame.math import Vector2

class MyEntity(Entity):
	def update(self):
		destination_pos = cell2pixels(Vector2(4, 1))
		if self.get_position() == destination_pos:
			# si l'entite est a la case X:4 Y:1
			APPLICATION.HANDLER.set_title("On est arrives !")
			\end{lstlisting}
		
		\section{Comment quitter le jeu ?}

			\textit{Comment peut-on déclancher la fermeture du jeu autrement qu'en fermant la fenêtre~?}

			Vous pouvez utiliser la méthode \verb|Application.exit()|, qui va mettre fin à la boucle principale du jeu. Le jeu terminera donc le tour de boucle en cours, puis déclanchera la fermeture propre du jeu.

			Exemple~: fermer le jeu quand on appuie sur échap~:

			\begin{lstlisting}[language=Python]
from nemesis.Entity import Entity
from nemesis.constants import *

import pygame

class KeyListener(Entity):
	def process_event(self, event):
		super().process_event(event)
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				APPLICATION.HANDLER.exit()
			\end{lstlisting}
\end{document}
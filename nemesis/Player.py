# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
from nemesis.Entity import *
from nemesis.constants import *
from nemesis.Map import Map
from nemesis.Map import pixels2cell

class Player(Entity):
	"""La classe qui représente le joueur."""
	def __init__(self):
		super().__init__()

	def process_event(self, event):
		if event.type == pygame.KEYDOWN:
			current_map: Map = APPLICATION.HANDLER.get_game_scenes()[APPLICATION.HANDLER.get_current_game_scene()].get_map()
			current_pos = pixels2cell(self.get_position())
			if event.key == pygame.K_LEFT:
				to_pos = pygame.math.Vector2(current_pos)
				to_pos.x -= 1 # La case où on veut aller
				tiles = current_map.get_tiles_at(to_pos) # les tiles de là où on veut aller
				tiles_collisions = [x.collisions_policy for x in tiles] # la liste des politiques de collision des tiles de la case où on veut aller
				if not (True in tiles_collisions): # si aucune tile n'a de boîte de collision
					if G_USE_SPRITES_ZOOM:
						self._position.x -= G_CELL_WIDTH * G_SPRITES_ZOOM
					else:
						self._position.x -= G_CELL_WIDTH
			elif event.key == pygame.K_RIGHT:
				to_pos = pygame.math.Vector2(current_pos)
				to_pos.x += 1
				tiles = current_map.get_tiles_at(to_pos)
				tiles_collisions = [x.collisions_policy for x in tiles]
				if not (True in tiles_collisions):
					if G_USE_SPRITES_ZOOM:
						self._position.x += G_CELL_WIDTH * G_SPRITES_ZOOM
					else:
						self._position.x += G_CELL_WIDTH
			elif event.key == pygame.K_UP:
				to_pos = pygame.math.Vector2(current_pos)
				to_pos.y -= 1
				tiles = current_map.get_tiles_at(to_pos)
				tiles_collisions = [x.collisions_policy for x in tiles]
				if not (True in tiles_collisions):
					if G_USE_SPRITES_ZOOM:
						self._position.y -= G_CELL_HEIGHT * G_SPRITES_ZOOM
					else:
						self._position.y -= G_CELL_HEIGHT
			elif event.key == pygame.K_DOWN:
				to_pos = pygame.math.Vector2(current_pos)
				to_pos.y += 1
				tiles = current_map.get_tiles_at(to_pos)
				tiles_collisions = [x.collisions_policy for x in tiles]
				if not (True in tiles_collisions):
					if G_USE_SPRITES_ZOOM:
						self._position.y += G_CELL_HEIGHT * G_SPRITES_ZOOM
					else:
						self._position.y += G_CELL_HEIGHT
	
	def update(self):
		super().update()
		# TODO DEBUG
		if APPLICATION.HANDLER is not None:
			current_scene = APPLICATION.HANDLER.get_game_scenes()[APPLICATION.HANDLER.get_current_game_scene()]
			current_map: Map = current_scene.get_map()
			#print([x.collisions_policy for x in current_map.get_tiles_at(pixels2cell(self.get_position()))])

	def render(self, surface: pygame.Surface):
		return super().render(surface)
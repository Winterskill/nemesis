# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame

class APPLICATION:
	HANDLER = None
	DELTA_TIME = 0

def internal_set_application(app):
	"""Définit sur quelle instance d'`Application` la constante globale
	`APPLICATION.HANDLER` doit pointer. Normalement, l'utilisateur du
	framework n'aura jamais à utiliser cette fonction."""
	APPLICATION.HANDLER = app
	return

def internal_set_delta_time(dt: float):
	"""Définit le delta time. Normalement, l'utilisateur du framework n'aura
	jamais à utiliser cette fonction."""
	APPLICATION.DELTA_TIME = dt
	return


# Error codes
ERR_NO_PYGAME_MODULE = -1

# Time
T_CLOCK = pygame.time.Clock()
T_FPS   = 60

# Files paths
F_DIR_SPRITES  = "resources/sprites"
F_DIR_TILESETS = "resources/tilesets"
F_DIR_MAPS     = "resources/maps"

# Grid & graphics
G_CELL_WIDTH       = 32
G_CELL_HEIGHT      = 32
# G_SPRITES_ZOOM
#   Définit le degré par lequel il faut multiplier la taille des sprites.
G_SPRITES_ZOOM     = 2.0
# G_USE_SPRITES_ZOOM
#   Si True, utilise la multiplication de taille des sprites.
G_USE_SPRITES_ZOOM = False
# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pygame
from nemesis.Listener import *
from nemesis.Renderer import *
from nemesis.Updater import *

class Scene(EventsListener, Renderer):
	"""La classe Scene représente une scène du jeu. Elle est la classe-mère
	des classes plus spécialisées pour la représentation des scènes."""
	def __init__(self, name: str):
		self.__name = name
	
	def get_name(self) -> str:
		"""Retourne le nom de la scène. Le nom de la scène doit être unique,
		mais il peut y avoir une GameScene et une MenuScene avec le même
		nom. Le nom est en lecture seule, il n'est pas possible de le
		modifier."""
		return self.__name
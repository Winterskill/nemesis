# Nemesis
# Copyright (C) 2020  Soni Tourret
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from nemesis.Application import *
from nemesis.GameScene import *
from nemesis.Player import *
from nemesis.Map import Map
from nemesis.Map import cell2pixels
from nemesis.constants import *

from pygame.math import Vector2
from pygame import Surface

class TestNPC(Entity):
	def render(self, surface: Surface):
		super().render(surface)
		sysfont = pygame.font.Font(None, 20)
		text = sysfont.render("Nemesis Test Demo", False, (255, 255, 255))
		text_height = text.get_height()
		surface.blit(text, (0, 0))
		text = sysfont.render("Version Alpha 2", False, (255, 255, 255))
		surface.blit(text, (0, text_height))
		text = sysfont.render("https://gitlab.com/SamayoManati/nemesis", False, (255, 255, 255))
		surface.blit(text, (0, 2 * text_height))

class KeyListener(Entity):
	def process_event(self, event):
		super().process_event(event)
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_ESCAPE:
				APPLICATION.HANDLER.exit()

if __name__ == '__main__':
	app = Application()

	player = Player()
	player.load_sprite("player")
	player.set_used_sprite("face")
	player.set_name("player")

	npc = TestNPC()
	npc.set_name("jacky")
	npc.load_sprite("tile.slab")
	npc.set_used_sprite("tile")
	npc.set_position(cell2pixels(Vector2(2, 5)))
	npc.set_destination(cell2pixels(Vector2(0,0)))

	keys_listener = KeyListener()

	carte = Map("carte")

	maScene = GameScene("ma_scene", carte)
	maScene.register_entity(player)
	maScene.register_entity(npc)
	maScene.register_entity(keys_listener)
	app.register_game_scene(maScene)
	app.set_current_game_scene("ma_scene")

	app.execute()